<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pipelines - Home</title>

    <style>
        body{
            padding-top: 50px;
            display: flex;
            justify-content: center;
        }
        .container{
            text-align: center;
            font-family: sans-serif;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Welkom bij de wel-les</h1>
        <p>We gaan het in deze les hebben over CI/CD via Bitbucket Pipelines</p>
    </div>
</body>
</html>